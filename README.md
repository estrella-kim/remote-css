Lazyside Front-end 개발자를 위한 CSS 규칙입니다.

### 1. css 모든 속성은 영문 소문자를 사용합니다.

```css
.lazy {
  width: 100px;
}
```

### 2. 선택자와 속성 값 사이에는 한칸을 띄어쓰기 하고 속성 값의 key와 value 값 사이는 한칸을 띄어씁니다. 또한 속성 값 부여 시 줄바꿈을 사용합니다.  

```css
.lazy {
  width: 100px;
}
```

```css
.lazy {
  width: 100px;
  height: 150px;
  margin: 0 auto;
}
```

### 3. 글꼴 선언 시 작은 따옴표를 @charset 사용시 큰 따옴표를 사용합니다.

```css
@charset "utf-8";
.lazy {
  font-family: '레이지사이드', 'lazy side';
}
```
### 4. css 인코딩시 HTML과 동일한 인코딩을 문서 첫줄에 선언합니다.
```css
@charset "utf-8";
```

### 5. 한글 글꼴 선언 시 한글과 영문 이름을 둘 다 적용합니다.
```css
.lazy {
  font-family: '레이지사이드', 'lazy side';
}
```

### 6. z-index는 최소 10 ~ 최대 1000까지 사용하고 10 단위로 증가시킵니다.
```css
.laze_1 {
  z-index: 10;
}
.laze_2 {
  z-index: 20;
}
.laze_3 {
  z-index: 30;
}
```



### 7. 각 해당 서브페이지에 맞는 css파일은 css폴더 내부에서 관리합니다.

```
[ 예시 ]



[festival]
  │
  ├[view]
  │  ├ login.html
  │  ├ notice.html
  │  └ introduction.html
  ├[css]
  │  ├ login.css
  │  ├ notice.css
  │  └ introduction.css
  ├[js]
  ├[static]
  └ index.html


```

### 8. 작성순서
> 각 클래스와 연결된 코드들끼리 정리합니다.
> dom요소의 순서대로 작성합니다.

### 9. 주석
> Doxygen을 이용한 주석 규칙을 사용합니다.
> dom의 큰 요소에 따라 한 줄 띄고 주석처리로 영역을 구분합니다.
> 영역이 끝난 후 닫아주는 태그를 기입하지 않아도 됩니다.
> author, description, date등의 주석설명이 붙는 경우 가시성을 위해 수정한 코드의 바로 위에 주석처리합니다.
> 주석처리시 양쪽으로 한 칸씩 띄어쓰기합니다.
## 주석 작성은 명령어 리스트에 명시하여 작성합니다.

```
/**
* @명령어
*/


/**
* @file require.config.js
* @brief 간략한 설명
* @details 자세한 설명
*/

```

```
/*  header */
.header{
  background-color:#fff;
}

/*  content */
.content{
  background-color:#fff;
}
.content .wrap{
  width: 1200px;
  margin: 0 auto;
}

/* footer */
.footer{
  background-color:#fff;
}

```

>1.  `@file` - 파일표시
>1.  `@author` - 작성자
>1.  `@brief` - 간략한 설명
>1.  `@details` - 자세한 설명
>1.  `@param` - 변수 설명
>1.  `@return` - 리턴 설명
>1.  `@see` - 참고사항, 주석을 볼 때 추가적으로 확인해야 할 사항들 작성
>1.  `@todo` - 추가적으로 처리해야할 사항들 작성
>1.  `@deprecated` - 삭제 예정 표시
>1.  작성순서는 `@file`부터 `@deprecated`까지 순서대로 작성하며 @file부터 @brief까지는 필수로 작성하고 나머지는 옵션으로 작성합니다.
